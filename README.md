# Project Archetype

An archetype is a very simple artifact, that contains the project prototype you wish to create.

## Guides
### Step 1:
Clone this project, command `cd` to the project and run command-line to install archetype to your local repository:
```shell script
mvn clean install
```
### Step 2:
Go to the folder you want to generate new project and run command-line.

>mvn archetype:generate -DarchetypeGroupId=${archetypeGroupId} -DarchetypeArtifactId=${archetypeArtifactId} -DarchetypeVersion=${archetypeVersion} -DgroupId=${groupId} -Dversion=${version} -DartifactId=${artifactId}
- archetypeGroupId: archetype group id
- archetypeArtifactId: archetype name (this example is project-archetype)
- archetypeVersion: archetype version
- groupId: project group id
- version: project version
- artifactId: project name

Example:
``` shell script
mvn archetype:generate -DarchetypeGroupId=com.tenxtenx.saladin -DarchetypeArtifactId=saladin-archetype -DarchetypeVersion=1.0.0 -DgroupId=com.tenxtenx.saladin -Dversion=1.0.0-SNAPSHOT -DartifactId=test-archetype
```