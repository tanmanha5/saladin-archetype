package com.tenxtenx.saladin.exception;

import com.tenxtenx.saladin.enums.EStatusResponse;
import com.tenxtenx.saladin.exception.base.SaladinException;
import org.springframework.http.HttpStatus;

import java.io.Serial;

public class BadRequestException extends SaladinException {

    @Serial
    private static final long serialVersionUID = 6164096114975634722L;

    private static final String DEFAULT_MESSAGE_BUNDLE = "response.bad.request";

    private static final EStatusResponse STATUS_CODE = EStatusResponse.BAD_REQUEST;
    private static final HttpStatus HTTP_STATUS = HttpStatus.BAD_REQUEST;

    public BadRequestException() {
        super(DEFAULT_MESSAGE_BUNDLE, STATUS_CODE.getValue(), HTTP_STATUS);
    }

    public BadRequestException(String messageBundle) {
        super(messageBundle, STATUS_CODE.getValue(), HTTP_STATUS);
    }

    public BadRequestException(String messageBundle, Object... args) {
        super(messageBundle, STATUS_CODE.getValue(), HTTP_STATUS, args);
    }

    public BadRequestException(Long status) {
        super(DEFAULT_MESSAGE_BUNDLE, status, HTTP_STATUS);
    }

    public BadRequestException(String messageBundle, Long status) {
        super(messageBundle, status, HTTP_STATUS);
    }

    public BadRequestException(String messageBundle, Long status, Object... args) {
        super(messageBundle, status, HTTP_STATUS, args);
    }
}
