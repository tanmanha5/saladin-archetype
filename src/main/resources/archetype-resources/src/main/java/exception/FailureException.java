package com.tenxtenx.saladin.exception;

import com.tenxtenx.saladin.enums.EStatusResponse;
import com.tenxtenx.saladin.exception.base.SaladinException;
import org.springframework.http.HttpStatus;

import java.io.Serial;

public class FailureException extends SaladinException {

    @Serial
    private static final long serialVersionUID = -3548271790809725362L;

    private static final String DEFAULT_MESSAGE_BUNDLE = "response.fail";

    private static final EStatusResponse STATUS_CODE = EStatusResponse.FAIL;

    private static final HttpStatus HTTP_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;

    public FailureException() {
        super(DEFAULT_MESSAGE_BUNDLE, STATUS_CODE.getValue(), HTTP_STATUS);
    }

    public FailureException(String messageBundle) {
        super(messageBundle, STATUS_CODE.getValue(), HTTP_STATUS);
    }

    public FailureException(String messageBundle, Object... args) {
        super(messageBundle, STATUS_CODE.getValue(), HTTP_STATUS, args);
    }

    public FailureException(Long status) {
        super(DEFAULT_MESSAGE_BUNDLE, status, HTTP_STATUS);
    }

    public FailureException(String messageBundle, Long status) {
        super(messageBundle, status, HTTP_STATUS);
    }

    public FailureException(String messageBundle, Long status, Object... args) {
        super(messageBundle, status, HTTP_STATUS, args);
    }
}
