package com.tenxtenx.saladin.dto.request;

import lombok.Data;

@Data
public class Request {

    public String email;
}
