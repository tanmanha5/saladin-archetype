package com.tenxtenx.saladin.dto.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tenxtenx.saladin.enums.ELanguage;
import com.tenxtenx.saladin.enums.EStatusResponse;
import com.tenxtenx.saladin.utils.MessageUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;

@Getter
@Setter
@Slf4j
@NoArgsConstructor
public class Response implements Serializable {

    @Serial
    private static final long serialVersionUID = 4543923542628633108L;

    private Long status;

    private String message;

    private Object data;

    public Response(Long status, String message) {
        this.status = status;
        this.message = message;
    }

    public Response(Long status, String message, Object data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public static Response success(ELanguage eLanguage, MessageSource messageSource) {
        return new Response(EStatusResponse.SUCCESS.getValue(), messageSource.getMessage("response.success", new Object[0], new Locale(eLanguage.name())));
    }

    public static Response badRequest(ELanguage eLanguage, MessageSource messageSource) {
        return new Response(EStatusResponse.BAD_REQUEST.getValue(), messageSource.getMessage("response.bad.request", new Object[0], new Locale(eLanguage.name())));
    }

    public static Response badRequest(ELanguage eLanguage, MessageSource messageSource, List<String> messageBundles) {
        var stringBuffer = new StringBuilder();
        messageBundles.forEach(messageBundle -> {
            String message;
            try {
                message = MessageUtil.toMessage(messageBundle, eLanguage, messageSource);
                stringBuffer.append(message).append(".");
            } catch (Exception exception) {
                log.error("Can't found message bundle: {}", messageBundle);
                stringBuffer.append(messageBundle);
            }
        });
        return new Response(EStatusResponse.BAD_REQUEST.getValue(), stringBuffer.toString());
    }

    public static Response systemError(ELanguage eLanguage, MessageSource messageSource) {
        return new Response(EStatusResponse.SYSTEM_ERROR.getValue(), messageSource.getMessage("response.system.error", new Object[0], new Locale(eLanguage.name())));
    }

    public static Response timeOut(ELanguage eLanguage, MessageSource messageSource) {
        return new Response(EStatusResponse.TIME_OUT.getValue(), messageSource.getMessage("response.time.out", new Object[0], new Locale(eLanguage.name())));
    }

    @Override
    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }

}
