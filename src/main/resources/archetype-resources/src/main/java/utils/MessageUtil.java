package com.tenxtenx.saladin.utils;

import com.tenxtenx.saladin.enums.ELanguage;
import org.springframework.context.MessageSource;

import java.util.Locale;

public class MessageUtil {

    private MessageUtil() {
    }

    public static String toMessage(String messageBundle, ELanguage language, MessageSource messageSource) {
        return messageSource.getMessage(messageBundle, new Object[0], new Locale(language.name()));
    }

    public static String toMessage(String messageBundle, ELanguage language, MessageSource messageSource, Object... args) {
        return messageSource.getMessage(messageBundle, args, new Locale(language.name()));
    }
}
