
package com.tenxtenx.saladin.utils;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.StringWriter;

public class AOPUtil {

    private AOPUtil() {
    }

    public static void appendThrowableText(StringBuilder sb, Throwable t) {
        if (t == null) {
            return;
        }
        StringWriter sw = new StringWriter(100);
        t.printStackTrace(new PrintWriter(sw));
        appendInformation2StringBuilder(sb, "exception-thrown", sw.toString());
    }

    public static void appendInformation2StringBuilder(StringBuilder sb, String description, String information2Append) {
        sb.append(description);
        sb.append(":");
        sb.append(information2Append);
        sb.append(";");
    }

    public static StringBuilder toError(HttpServletRequest request, Throwable throwable) {
        StringBuilder sb = new StringBuilder();
        sb.append(ReflectionToStringBuilder.toString(request));
        appendThrowableText(sb, throwable);
        return sb;
    }

}
