package com.tenxtenx.saladin.enums;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum ELanguage {
    vi,
    en;

    @JsonCreator
    public static ELanguage fromValue(String value) {
        return "en".equals(value) ? en : vi;
    }
}
