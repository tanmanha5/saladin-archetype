package com.tenxtenx.saladin.controller.advice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tenxtenx.saladin.constants.ConstantsValues;
import com.tenxtenx.saladin.dto.response.Response;
import com.tenxtenx.saladin.enums.ELanguage;
import com.tenxtenx.saladin.exception.base.SaladinException;
import com.tenxtenx.saladin.utils.ResponseEntityUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.tenxtenx.saladin.utils.AOPUtil.toError;

@ControllerAdvice
@Slf4j
public class RestControllerErrorAdvice {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ObjectMapper objectMapper;

    @ExceptionHandler(value = SaladinException.class)
    public ResponseEntity<Response> handleMerchantRuntimeException(HttpServletRequest request, SaladinException exception) {
        String language = Optional.ofNullable(request.getParameter(ConstantsValues.LANGUAGE_PARAMETER))
                .orElseGet(() -> (String) request.getAttribute(ConstantsValues.LANGUAGE_PARAMETER));
        ResponseEntity<Response> responseEntity = exception.getResponseEntity(ELanguage.fromValue(language), messageSource);
        logError(request, exception);
        logResponse(request, responseEntity);
        return responseEntity;
    }

    @ExceptionHandler(value = Throwable.class)
    public ResponseEntity<Response> handleException(HttpServletRequest request, Throwable t) throws JsonProcessingException {
        String language = Optional.ofNullable(request.getParameter(ConstantsValues.LANGUAGE_PARAMETER))
                .orElseGet(() -> (String) request.getAttribute(ConstantsValues.LANGUAGE_PARAMETER));
        var responseEntity = ResponseEntityUtil.getResponseEntity(t, ELanguage.fromValue(language), messageSource, objectMapper);
        logError(request, t);
        logResponse(request, responseEntity);
        return responseEntity;
    }

    @ExceptionHandler(value = BindException.class)
    public ResponseEntity<Response> handleException(HttpServletRequest request, BindException ex) {
        List<String> errors = ex.getBindingResult()
                .getAllErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
        var language = ELanguage.fromValue(request.getParameter(ConstantsValues.LANGUAGE_PARAMETER));
        var responseEntity = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Response.badRequest(language, messageSource, errors));
        logError(request, ex);
        logResponse(request, responseEntity);
        return responseEntity;
    }

    private void logError(HttpServletRequest request, Throwable throwable) {
        log.error("Unexpected Throwable caught by ControllerAdvice: {}", toError(request, throwable));
    }

    private void logResponse(HttpServletRequest request, ResponseEntity<Response> responseEntity){
        log.info("-----END [{}]{} | Response: {}", request.getMethod(), request.getRequestURI(), responseEntity.getBody());
    }
}
