package com.tenxtenx.saladin.controller;

import com.tenxtenx.saladin.dto.request.Request;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/home")
public class HomeController {

    @PostMapping("/log")
    public void homeController(@RequestBody Request request) {
        log.info("----- POST request {} ", request);
    }
}
