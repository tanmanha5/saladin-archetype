package com.tenxtenx.saladin.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tenxtenx.saladin.convert.StatusResponseDeserializer;
import com.tenxtenx.saladin.convert.StatusResponseSerializer;

import java.util.Objects;

@JsonDeserialize(using = StatusResponseDeserializer.class)
@JsonSerialize(using = StatusResponseSerializer.class)
public enum EStatusResponse {

    SUCCESS(0L),
    BAD_REQUEST(1L),
    FAIL(6L),
    TOKEN_EXPIRED(21L),
    TOKEN_INVALID(22L),
    UNAUTHORIZED(78L),
    TIME_OUT(45L),
    EXIST(18L),
    SYSTEM_ERROR(1006L),
    NOT_FOUND(79L);

    private final Long status;

    @JsonValue
    public Long getValue() {
        return this.status;
    }

    EStatusResponse(Long statusCode) {
        this.status = statusCode;
    }

    @JsonCreator
    public static EStatusResponse fromValue(Long value) {
        for (EStatusResponse status : EStatusResponse.values()) {
            if (Objects.equals(status.getValue(), value)) {
                return status;
            }
        }
        return null;
    }
}
