package com.tenxtenx.saladin.convert;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.tenxtenx.saladin.enums.EStatusResponse;

import java.io.IOException;

public class StatusResponseSerializer extends JsonSerializer<EStatusResponse> {

    @Override
    public void serialize(EStatusResponse value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeNumber(value.getValue());
    }
}
