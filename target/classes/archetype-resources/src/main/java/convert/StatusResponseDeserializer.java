package com.tenxtenx.saladin.convert;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.tenxtenx.saladin.enums.EStatusResponse;

import java.io.IOException;

public class StatusResponseDeserializer extends JsonDeserializer<EStatusResponse> {

    @Override
    public EStatusResponse deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        int statusCode = node.asInt();
        for (EStatusResponse status : EStatusResponse.values()) {
            if (statusCode == status.getValue()) {
                return status;
            }
        }
        return null;
    }
}
