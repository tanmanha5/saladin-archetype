package com.tenxtenx.saladin.constants;

public class ConstantsValues {

    public static final String EMPTY_STRING = "";

    public static final String LANGUAGE_PARAMETER = "language";

    public static final String DEFAULT_LANGUAGE = "#{T(com.mservice.merchant.starter.utility.enums.ELanguage).vi}";
}
