package com.tenxtenx.saladin.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tenxtenx.saladin.dto.response.Response;
import com.tenxtenx.saladin.enums.ELanguage;
import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

@Slf4j
public class ResponseEntityUtil {


    private static final HttpStatus DEFAULT_ERROR_STATUS_CODE = HttpStatus.INTERNAL_SERVER_ERROR;

    private ResponseEntityUtil() {
    }

    public static ResponseEntity<Response> getResponseEntity(Throwable throwable, ELanguage eLanguage, MessageSource messageSource, ObjectMapper objectMapper) throws JsonProcessingException {

        //Response System Error
        if (throwable == null
                || throwable instanceof NullPointerException
                || throwable instanceof NumberFormatException) {
            return ResponseEntity.status(DEFAULT_ERROR_STATUS_CODE).body(Response.systemError(eLanguage, messageSource));
        }

        // Response Timeout
        if (throwable instanceof TimeoutException
                || throwable instanceof IOException
                || throwable instanceof InterruptedException) {
            return ResponseEntity.status(HttpStatus.REQUEST_TIMEOUT).body(Response.timeOut(eLanguage, messageSource));
        }

        // Response Bad request
        if (throwable instanceof IllegalArgumentException
                || throwable instanceof HttpMessageNotReadableException
                || throwable instanceof ConstraintViolationException
                || throwable instanceof MethodArgumentTypeMismatchException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Response.badRequest(eLanguage, messageSource));
        }

        // Exception when call another service via Openfeign.
        if (throwable instanceof FeignException) {
            var exception = (FeignException) throwable;
            var response = objectMapper.readValue(exception.contentUTF8(), Response.class);
            return ResponseEntity.status(exception.status()).body(response);
        }

        // Exception when call another service via Rest Template.
        if (throwable instanceof HttpClientErrorException) {
            var exception = (HttpClientErrorException) throwable;
            var response = objectMapper.readValue(exception.getMessage(), Response.class);
            return ResponseEntity.status(exception.getStatusCode()).body(response);
        }

        // For every other Throwable, return an INTERNAL_SERVER_ERROR
        return ResponseEntity.status(DEFAULT_ERROR_STATUS_CODE).body(Response.systemError(eLanguage, messageSource));
    }


}
