package com.tenxtenx.saladin.exception;

import com.tenxtenx.saladin.enums.EStatusResponse;
import com.tenxtenx.saladin.exception.base.SaladinException;
import org.springframework.http.HttpStatus;

import java.io.Serial;


public class TimeOutException extends SaladinException {

    @Serial
    private static final long serialVersionUID = 8974978610425150943L;

    private static final String DEFAULT_MESSAGE_BUNDLE = "response.system.error";

    private static final EStatusResponse STATUS_CODE = EStatusResponse.SYSTEM_ERROR;

    private static final HttpStatus HTTP_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;

    public TimeOutException() {
        super(DEFAULT_MESSAGE_BUNDLE, STATUS_CODE.getValue(), HTTP_STATUS);
    }

    public TimeOutException(String messageBundle) {
        super(messageBundle, STATUS_CODE.getValue(), HTTP_STATUS);
    }

    public TimeOutException(String messageBundle, Object... args) {
        super(messageBundle, STATUS_CODE.getValue(), HTTP_STATUS, args);
    }

    public TimeOutException(Long status) {
        super(DEFAULT_MESSAGE_BUNDLE, status, HTTP_STATUS);
    }

    public TimeOutException(String messageBundle, Long status) {
        super(messageBundle, status, HTTP_STATUS);
    }

    public TimeOutException(String messageBundle, Long status, Object... args) {
        super(messageBundle, status, HTTP_STATUS, args);
    }
}
