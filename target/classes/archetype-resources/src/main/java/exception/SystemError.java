package com.tenxtenx.saladin.exception;

import com.tenxtenx.saladin.enums.EStatusResponse;
import com.tenxtenx.saladin.exception.base.SaladinException;
import org.springframework.http.HttpStatus;

import java.io.Serial;

public class SystemError extends SaladinException {

    @Serial
    private static final long serialVersionUID = -1313899708642783593L;

    private static final String DEFAULT_MESSAGE_BUNDLE = "response.system.error";

    private static final EStatusResponse STATUS_CODE = EStatusResponse.SYSTEM_ERROR;

    private static final HttpStatus HTTP_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;

    public SystemError() {
        super(DEFAULT_MESSAGE_BUNDLE, STATUS_CODE.getValue(), HTTP_STATUS);
    }

    public SystemError(String messageBundle) {
        super(messageBundle, STATUS_CODE.getValue(), HTTP_STATUS);
    }

    public SystemError(String messageBundle, Object... args) {
        super(messageBundle, STATUS_CODE.getValue(), HTTP_STATUS, args);
    }

    public SystemError(Long status) {
        super(DEFAULT_MESSAGE_BUNDLE, status, HTTP_STATUS);
    }

    public SystemError(String messageBundle, Long status) {
        super(messageBundle, status, HTTP_STATUS);
    }

    public SystemError(String messageBundle, Long status, Object... args) {
        super(messageBundle, status, HTTP_STATUS, args);
    }
}
