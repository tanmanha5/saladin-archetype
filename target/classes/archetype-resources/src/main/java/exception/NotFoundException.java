package com.tenxtenx.saladin.exception;

import com.tenxtenx.saladin.enums.EStatusResponse;
import com.tenxtenx.saladin.exception.base.SaladinException;
import org.springframework.http.HttpStatus;

import java.io.Serial;

public class NotFoundException extends SaladinException {

    @Serial
    private static final long serialVersionUID = -1337104658400191070L;

    private static final String DEFAULT_MESSAGE_BUNDLE = "response.not.found";

    private static final EStatusResponse STATUS_CODE = EStatusResponse.NOT_FOUND;

    private static final HttpStatus HTTP_STATUS = HttpStatus.NOT_FOUND;

    public NotFoundException() {
        super(DEFAULT_MESSAGE_BUNDLE, STATUS_CODE.getValue(), HTTP_STATUS);
    }

    public NotFoundException(String messageBundle) {
        super(messageBundle, STATUS_CODE.getValue(), HTTP_STATUS);
    }

    public NotFoundException(String messageBundle, Object... args) {
        super(messageBundle, STATUS_CODE.getValue(), HTTP_STATUS, args);
    }

    public NotFoundException(Long status) {
        super(DEFAULT_MESSAGE_BUNDLE, status, HTTP_STATUS);
    }

    public NotFoundException(String messageBundle, Long status) {
        super(messageBundle, status, HTTP_STATUS);
    }

    public NotFoundException(String messageBundle, Long status, Object... args) {
        super(messageBundle, status, HTTP_STATUS, args);
    }
}
