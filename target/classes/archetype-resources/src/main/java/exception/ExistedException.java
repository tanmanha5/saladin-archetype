package com.tenxtenx.saladin.exception;

import com.tenxtenx.saladin.enums.EStatusResponse;
import com.tenxtenx.saladin.exception.base.SaladinException;
import org.springframework.http.HttpStatus;

import java.io.Serial;

public class ExistedException extends SaladinException {

    @Serial
    private static final long serialVersionUID = 641134053034935367L;
    private static final String DEFAULT_MESSAGE_BUNDLE = "response.existed";
    private static final EStatusResponse STATUS_CODE = EStatusResponse.EXIST;
    private static final HttpStatus HTTP_STATUS = HttpStatus.FOUND;


    public ExistedException() {
        super(DEFAULT_MESSAGE_BUNDLE, STATUS_CODE.getValue(), HTTP_STATUS);
    }

    public ExistedException(String messageBundle) {
        super(messageBundle, STATUS_CODE.getValue(), HTTP_STATUS);
    }

    public ExistedException(String messageBundle, Object... args) {
        super(messageBundle, STATUS_CODE.getValue(), HTTP_STATUS, args);
    }

    public ExistedException(Long status) {
        super(DEFAULT_MESSAGE_BUNDLE, status, HTTP_STATUS);
    }

    public ExistedException(String messageBundle, Long status) {
        super(messageBundle, status, HTTP_STATUS);
    }

    public ExistedException(String messageBundle, Long status, Object... args) {
        super(messageBundle, status, HTTP_STATUS, args);
    }
}
