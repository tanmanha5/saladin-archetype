package com.tenxtenx.saladin.exception.base;

import com.tenxtenx.saladin.dto.response.Response;
import com.tenxtenx.saladin.enums.ELanguage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.Serial;
import java.util.Locale;

@Slf4j
public class SaladinException extends Exception {

    @Serial
    private static final long serialVersionUID = 5746835089414085960L;


    protected final String messageBundle;

    protected final Object[] args;

    protected final Long status;

    protected final HttpStatus httpStatus;

    protected SaladinException(String messageBundle, Long status, HttpStatus httpStatus) {
        this.args = new Object[0];
        this.messageBundle = messageBundle;
        this.status = status;
        this.httpStatus = httpStatus;
    }

    protected SaladinException(String messageBundle, Long status, HttpStatus httpStatus, Object... args) {
        this.args = args;
        this.messageBundle = messageBundle;
        this.status = status;
        this.httpStatus = httpStatus;
    }

    public ResponseEntity<Response> getResponseEntity(ELanguage eLanguage, MessageSource messageSource) {
        String message;
        try {
            message = messageSource.getMessage(this.messageBundle, this.args, new Locale(eLanguage.name()));
        } catch (Exception exception) {
            log.error("Can't found message bundle: {}", messageBundle);
            message = this.messageBundle;
        }
        return new ResponseEntity<>(new Response(this.status, message), this.httpStatus);
    }

}
