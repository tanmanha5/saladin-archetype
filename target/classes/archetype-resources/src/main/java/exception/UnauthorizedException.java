package com.tenxtenx.saladin.exception;

import com.tenxtenx.saladin.enums.EStatusResponse;
import com.tenxtenx.saladin.exception.base.SaladinException;
import org.springframework.http.HttpStatus;

import java.io.Serial;

public class UnauthorizedException extends SaladinException {

    @Serial
    private static final long serialVersionUID = -2561939691151488407L;

    private static final String DEFAULT_MESSAGE_BUNDLE = "response.access.denied";

    private static final EStatusResponse STATUS_CODE = EStatusResponse.UNAUTHORIZED;

    private static final HttpStatus HTTP_STATUS = HttpStatus.UNAUTHORIZED;

    public UnauthorizedException() {
        super(DEFAULT_MESSAGE_BUNDLE, STATUS_CODE.getValue(), HTTP_STATUS);
    }

    public UnauthorizedException(String messageBundle) {
        super(messageBundle, STATUS_CODE.getValue(), HTTP_STATUS);
    }

    public UnauthorizedException(String messageBundle, Object... args) {
        super(messageBundle, STATUS_CODE.getValue(), HTTP_STATUS, args);
    }

    public UnauthorizedException(Long status) {
        super(DEFAULT_MESSAGE_BUNDLE, status, HTTP_STATUS);
    }

    public UnauthorizedException(String messageBundle, Long status) {
        super(messageBundle, status, HTTP_STATUS);
    }

    public UnauthorizedException(String messageBundle, Long status, Object... args) {
        super(messageBundle, status, HTTP_STATUS, args);
    }
}
